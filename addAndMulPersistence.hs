import Data.Char
import Data.List

numToDigits :: Int -> [Int]
numToDigits n = map digitToInt $ show n

additive :: Int -> Int
additive n = sum (numToDigits n)

multiplicative :: Int -> Int
multiplicative n = product (numToDigits n)

sumTilDigit :: Int -> Int
sumTilDigit n = if (length(numToDigits (additive n))) > 1
                 then 1 + sumTilDigit (additive n)
              else 1

productTilDigit :: Int -> Int
productTilDigit n = if (length (numToDigits (multiplicative n))) > 1
                   then 1 + productTilDigit (multiplicative n)
                  else 1

main = do
       print $ sumTilDigit 1679583
       print $ productTilDigit 77 
