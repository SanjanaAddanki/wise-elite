def searchRange(nums, target):
        numsStr = ''.join([str(i) for i in nums])
        tarList = []
        index = 0
        while index < len(numsStr):
            ind = numsStr.find(str(target), index)
            if ind != -1:
                tarList.append(ind)
            else :
                break
            index = ind + 1
        if len(tarList) == 0:
            return [-1, -1]
        else :
            return tarList
print(searchRange([1], 1))