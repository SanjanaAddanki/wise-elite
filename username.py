n = int(input())
li = [int(i) for i in input().split()]
count = 0
for i in range(len(li) - 1):
    if li[i + 1] > li[i]:
        count += 1
if len(li) == 2 and count == 0:
    count = 1
print(count)