word = input()
vowels = "aeiouy"
result = ""
for i in word.lower():
    if i not in vowels:
        result += "." + i
print(result)