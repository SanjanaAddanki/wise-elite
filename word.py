s = input()
upCount = len([1 for i in s if i.isupper()])
loCount = len([1 for i in s if i.islower()])
if loCount >= upCount:
    print(s.lower())
else :
    print(s.upper())