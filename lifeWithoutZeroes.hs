import Data.Char

digits :: Int -> [Int]
digits n = map digitToInt $ show n

joins n = read (map intToDigit n) :: Int

removeZeroes n = [x | x <- digits n, x /= 0]

withoutZeroes a b = joins (removeZeroes a) + joins (removeZeroes b)

main = do
       a <- readLn
       b <- readLn
       if joins(removeZeroes (a + b)) == withoutZeroes a b
       then putStrLn $ id  "YES"
       else putStrLn $ id  "NO"
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
-- INSERT --                                                 17,17         All
