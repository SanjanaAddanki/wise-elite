# your code goes here
testcases = int(input())
for i in range(testcases):
    x, y = map(int, input().split())
    base = int(str(x)[-1])
    exp = y % 4
    if exp == 0:
        exp = 4
    print(int(str(base ** exp)[-1]))