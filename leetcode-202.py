n = 7
if n == 7:
    n = 1
while n >= 9:
    n = sum(map(lambda k : k * k, [int(d) for d in str(n)]))
print(n == 1)