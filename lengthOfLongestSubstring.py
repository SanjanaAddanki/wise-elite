def allUnique(s, i, j):
    uniqueSet = set()
    for k in range(i, j):
        ch = s[k]
        if ch in uniqueSet:
            return False
        uniqueSet.add(ch)
    return True
def lengthOfLongestSubstring(s):
    sol = 0
    for i in range(0, len(s)):
        for j in range(i + 1, len(s)):
            if allUnique(s, i, j) :
                sol = max(sol, j - i)
    return sol
print(lengthOfLongestSubstring("abcabcbb"))