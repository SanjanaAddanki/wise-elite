def longestCommonPrefix(strs):
    li = [strs[0][: i + 1] for i in range(len(strs[0]))]
    strs.remove(strs[0])
    tarList = [j for i in strs for j in li if i.startswith(j) and len(j) >= 2]
    k = [(tarList.count(i), i) for i in tarList]
    return max(k)[1] if k else ""
strs = ["flower","flow","flag"]
print(longestCommonPrefix(strs))