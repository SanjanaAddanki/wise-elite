def longest_palindromic_string(word):
  max_pal = ''
  len_max_pal = 0
  for i in range(len(word)):
    if word[:i + 1] == ''.join(reversed(word[:i + 1])) and len_max_pal < len(word[:i + 1]):
      len_max_pal = len(word[:i + 1])
      max_pal = word[:i + 1]
    for j in range(i, len(word) + 1):
      if word[i:j] == ''.join(reversed(word[i:j])) and len_max_pal < len(word[i : j]):
        len_max_pal = len(word[i : j])
        max_pal = word[i : j]
  print(max_pal)
longest_palindromic_string('cbbd')