def golfProblem():
    courseScores = [4, 4, 5, 3, 4, 4, 3, 5, 5, 3, 5, 4, 4, 4, 4, 3, 4, 4]
    golfTerms = {'albatross': -3, 'eagle' : -2, 'birdie' : -1, 'par' : 0,'bogey':1, 'double-bogey':2, 'triple-bogey':3}
    playerScores = ['eagle', 'bogey', 'par', 'bogey', 'double-bogey', 'birdie','bogey','par', 'birdie', 'par', 'par', 'par', 'par', 'par','bogey', 'eagle', 'bogey', 'par']
    courseScoresSum = sum(courseScores)
    playerScoresSum = sum([courseScores[i] + golfTerms[playerScores[i]] for i in range(len(playerScores))])
    if playerScoresSum > courseScoresSum :
        return '{} over par'.format(playerScoresSum - courseScoresSum)
    if playerScoresSum < courseScoresSum :
        return '{} over par'.format(courseScoresSum - playerScoresSum)
    else :
        return courseScoresSum
    
print(golfProblem())