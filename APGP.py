nums = [int(i) for i in input().split()]
target = []
while nums[0] != 0 :
    numsDiff, numsDiv = [], []
    for i in range(len(nums) - 1):
        numsDiff.append(abs(nums[i] - nums[i + 1]))
        numsDiv.append(nums[i + 1] / nums[i])
    if len(set(numsDiff)) == 1:
        target.append(("AP", nums[-1] + numsDiff[0]))
    if len(set(numsDiv)) == 1:
        target.append(("GP", int(nums[-1] * numsDiv[0])))
    nums.clear()
    nums = [int(i) for i in input().split()]
for i in target:
    print(i[0], i[1])