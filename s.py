from itertools import permutations
s = input()
steps = 0
for i in list(permutations([i for i in range(1, len(s) + 1)], 2)):
    if i[0] < i[1] and s[i[0] - 1] == 'U':
        steps += 1
    if i[0] < i[1] and s[i[0] - 1] == 'D':
            steps += 2 * s[i[0] - 1:].count('U') 
    if i[0] > i[1] and s[i[0] - 1] == 'U':
            steps += 2 *  s[i[0] - 1:].count('D') 
    if i[0] > i[1] and s[i[0] - 1] == 'D':
        steps += 1
print(steps )