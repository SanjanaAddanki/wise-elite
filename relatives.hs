isPrime :: Int -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime n = length [i | i <- [2..(round(sqrt(fromIntegral n)))], mod n i == 0] == 0

getCountOfRelatives :: Int -> Int
getCountOfRelatives n = if isPrime n
                        then  n - 1
                        else  length([1 | i <- [1..n], ((gcd n i) == 1)])
main = do
       num <- getLine
       let n = (read num :: Int)
       if n /= 0
          then do
               print $ getCountOfRelatives n
               main
       else return ()
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
~                                                                              
"relatives.hs" 17L, 511C                                     1,1           All
