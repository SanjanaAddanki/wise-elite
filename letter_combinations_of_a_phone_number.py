import itertools
def map_num_to_letters(num):
    given_dict = {2 : 'abc', 3 : 'def', 4 : 'ghi', 5 : 'jkl', 6 : 'mno', 7 : 'pqrs', 8 : 'tuv', 9 : 'wxyz'}
    for i in given_dict:
        if i == num:
            return given_dict[i]
def letter_combinations(word):
    li = [map_num_to_letters(int(i)) for i in word]
    for j in itertools.product(li[0], li[1], li[2]):
        print(j)
letter_combinations('234')